import os
import yaml

try:
    with open('ENV.yaml', 'r') as f:
        keys = yaml.load(f)
except IOError:
    keys = os.environ

OAUTH_TOKEN = keys['OAUTH_TOKEN']
OAUTH_TOKEN_SECRET = keys['OAUTH_TOKEN_SECRET']
CONSUMER_KEY = keys['CONSUMER_KEY']
CONSUMER_SECRET = keys['CONSUMER_SECRET']
