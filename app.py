from flask import Flask, request, session, render_template, g, redirect, url_for, flash, jsonify
import twitter_requests
import os
import jinja2
import random
import json
import twitter

app = Flask(__name__)

@app.route("/")
def default():

	try:

		sf_id = 2487956

		sf_trends = twitter_requests.ranked_trend_list(sf_id)

		sf_trends_dict = {}

		sf_trends_dict["San Francisco"] = sf_trends

		print "SUCCESS!"

		return render_template("index.html", sf_trends_dict=sf_trends_dict)

	except twitter.api.TwitterHTTPError:

		error_message = "Rate limit exceeded. Please try again."

		print error_message

		return render_template("index_error.html", error_message=error_message)

@app.route("/trends")
def get_trends():

	try:

		city = request.args.get("city")
		trends = twitter_requests.ranked_trend_list(city)

		return jsonify(trends)

	except twitter.api.TwitterHTTPError:

		error_message = "Rate limit exceeded. Please try again."

		print error_message

		return render_template("index_error.html", error_message=error_message)

@app.route("/status")
def get_tweets():

	trend = request.args.get("trend")
	max_id = request.args.get("tweet_id")
	tweets = twitter_requests.trend_tweets(trend, max_id)

	trend_tweets = []

	tweet_entities = ()

	for item in tweets["statuses"]:
		tweet_entities = (item["user"]["screen_name"], item["text"], item["id_str"])
		trend_tweets.append(tweet_entities)

	if not trend_tweets:
		error_message = "No more tweets to retrieve at the moment. Please try again."
	else:
		error_message = None

	tweet_id = tweets["search_metadata"]["max_id_str"]

	return render_template("status.html", trend_tweets=trend_tweets, tweet_id=tweet_id, trend=trend, error_message=error_message)

@app.route("/us")
def get_us_trends():

	try:

		us = request.args.get("versus")
		us_trends = twitter_requests.ranked_trend_list(us)

		return jsonify(us_trends)

	except twitter.api.TwitterHTTPError:

		error_message = "Rate limit exceeded. Please try again."

		print error_message

		return render_template("index_error.html", error_message=error_message)

@app.route("/tweet_id")
def get_more_tweets():
	tweet_id = request.args.get("more_tweets")
	more_tweets = twitter_requests.trend_tweets(tweet_id)

if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0")
