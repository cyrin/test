""" Making a keyword dictionary from a csv file """

keyword_file = open("keyword-dict.csv")
keyword_dict = {}
keyword_values = []

def make_dict():

	for line in keyword_file:
		keyword_entry = line.rstrip().split(',')
		keyword_dict[keyword_entry[0]] = keyword_entry[1:((len(keyword_entry)) - 2)]

	# for k, v in keyword_dict.items():
	# 	for i in v:
	# 		if i == '':
	# 			v.remove(i)

	return keyword_dict

# x = make_dict()

# print "########"

# for k, v in keyword_dict.items():
# 	print v
# 	for i in v:
# 		print i
# 		if i == '':
# 			v.remove(i)
# 			print v

# print "########"

# print
# print '**********'
# print x.keys()
# print x['murder']
# print x['protest']
# print x['death']

