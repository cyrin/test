trendVs: where & what people are tweeting
===========

Formerly the social media manager for <a href="http://www.dogster.com/author/liz-acosta" target="_blank">Dogster</a> and <a href="http://www.catster.com/author/liz-acosta" target="_blank">Catster</a> (yes -- for real), I wanted to create a tool to solve real world problems I had encountered on the job.

**trendVs** merges the dynamic world of **Twitter** with the power of **D3 data visualization**. With the ability to compare Twitter trends locally vs. nationally, trendVs enables brands to **access**, **identify**, and **capitalize** on **social media moments** that can help target and expand their reach and engagement.

With a single click, a user can see Twitter's latest trends with a beautiful and accessible visualization. With another click, users can search for intersecting popular topics between their city of choice and the rest of the country.

<a href="http://www.linkedin.com/in/lizacostalinkedin" target="_blank">Check me out on LinkedIn</a> or <a href="https://twitter.com/LizA_from_LA" target="_blank">follow me on Twitter</a> ... if you dare.

![trendVs_gif](static/trendvs-walkthrough.gif)

###Contents

- [What does it do?](#what-does-it-do)
- [How does it do it?](#how-does-it-do-it)
- [Do it yourself](#do-it-yourself)
- [What will it do?](#what-will-it-do)

###What does it do

- **Trends**: trendVs enables users to view the top ten Twitter trends for a selected US city.
- **Data Visualization**: With the power of <a href="http://d3js.org/" target="_blank">D3</a>, trendVs creates an accessible visual ranking of the top Twitter trends from most to least popular as indicated by size and color.
- **Tweets**: Clicking on a trend allows users to view a mix of real time and popular tweets associated with the keyword.
- **Comparison**: The "vs the United States" button displays two D3 charts, allowing users to easily find overlapping trends locally and nationally.

Back to **[Contents](#contents)**

![trendVs_screenshot](static/trendvs-fullscreen-1.png)

###How does it do it

- **Twitter API & OAuth**: Using OAuth, trendVs makes requests via the Twitter API through a Python wrapper that returns objects for the Twitter trends and tweets.
- **D3 JavaScript Library**: The object returned by the API call is then given to the D3 function that parses and binds the data to SVG, appending them to an element within the HTML.
- **Frontend**: Through a concert of HTML, jQuery, Jinja, JavaScript, and CSS, trendVs focuses on an intuitive user experience, the pleasure of discovery, and a bold consistent aesthetic.

![trendVs_D3](static/trendvs-d3.png)

Back to **[Contents](#contents)**

###Do it yourself

- **app.py**: The heart of trendVs, hosting the Flask server and routes, calling functions from **twitter_requests.py** and feeding the returned data to the HTML.
- **twitter_requests.py**: Uses <a href="http://mike.verdone.ca/twitter/" target="_blank">Python Twitter Tools</a> to make the Twitter API calls within a Python script.
- **d3_function.js**: Contains the D3 function that creates the trend bubble graphs.
- **working.py**: Referenced but not present in the repo, this file contains the access tokens required for OAuth. In order to duplicate trendVs locally, please see the <a href="https://dev.twitter.com/oauth/overview" target="_blank">Twitter API documentation</a>.

Back to **[Contents](#contents)**

###What will it do

- **Indicate Overlapping Trends**: Trends appearing both locally and nationally will be indicated on the bubble chart with a specific color.
- **Analysis & Alert**: Will search trend tweets for keywords indicating notable events, and when detected, will alert a registered user via text message.

![trendVs_comparison](static/trendvs-d3-comparison.png) 

Back to **[Contents](#contents)**
