// Dictionary of hard coded woeid and associated city names
locationDict = {2441472: 'Long Beach', 2367105: 'Boston', 2383489: 'Colorado Springs', 2423945: 'Honolulu', 2508428: 'Tucson', 2520077: 'Wichita', 2490383: 'Seattle', 2449808: 'Mesa', 2487956: 'San Francisco', 2452078: 'Minneapolis', 2427032: 'Indianapolis', 2357536: 'Austin', 2391585: 'Detroit', 2381475: 'Cleveland', 2358820: 'Baltimore', 2475687: 'Portland', 2449323: 'Memphis', 2383660: 'Columbus', 2471217: 'Philadelphia', 2379574: 'Chicago', 2428344: 'Jacksonville', 2378426: 'Charlotte', 2424766: 'Houston', 2442047: 'Los Angeles', 2406080: 'Fort Worth', 2388929: 'Dallas', 2486340: 'Sacramento', 2352824: 'Albuquerque', 2464592: 'Oklahoma City', 2487889: 'San Diego', 2457170: 'Nashville-Davidson', 2430683: 'Kansas City', 2407517: 'Fresno', 2471390: 'Phoenix', 2463583: 'Oakland', 2436704: 'Las Vegas', 2355942: 'Arlington', 2450022: 'Miami', 2458833: 'New Orleans', 2465512: 'Omaha', 2488042: 'San Jose', 2459115: 'New York', 2451822: 'Milwaukee', 2391279: 'Denver', 2487796: 'San Antonio', 2508533: 'Tulsa', 2397816: 'El Paso', 2357024: 'Atlanta', 2512636: 'Virginia Beach', 2514815: 'Washington'}

// On click action for city specific trends
$("#button").on("click", function(evt) {
	evt.preventDefault()
	d3.selectAll("#svg").remove();
	$("#tweets").html("");
	$("#tweets").css("padding-top","0%");
	$("#tweets").css("display", "none");

	var input = {}

	var cityId = $("select[name='city']").val();

	var cityName = locationDict[cityId];

	$.get("/trends?city=" + cityId, function( data) {

		console.log(data);

		input[cityName] = data;

		makeMagic(input, 600, "35px");
		});
	});

// On click action for US trends
$("#versus-button").on("click", function(evt) {
	evt.preventDefault()
	d3.selectAll("#svg").remove();
	$("#tweets").html("");
	$("#tweets").css("padding-top","10%");
	$("#tweets").css("display", "none");

	var cityInput = {}
	var usInput = {}
	var inputList = []

	// Gets data from #button
	var cityId = $( "select[name='city']" ).val();

	$.get("/trends?city=" + cityId, function( data ) {

		var cityName = locationDict[cityId];
		cityInput[cityName] = data;
		});
	
	var usId = $("button[name='versus']").val();

	$.get("/us?versus=" + usId, function(data) {

		var cityName = "vs the United States";
		usInput[cityName] = data;

		inputList.push(cityInput, usInput);

		// Iterates through array of trends
		inputList.forEach(function(item) {

			makeMagic(item, 400, "25px");
			});
		})
	});

// Function for making D3
function makeMagic(input, diameter, titleSize) {

	console.log(input);
	console.log("Magic happened!");

	for(var key in input) {
		var title = key};

	var json = input;

	// Color scale 
	var colorScale = d3.scale.linear()
			.range(["#00ffff", "#ff0000"]) // or use hex values
			.domain([1, 10]);

	// Selects where the D3 will go in index.html, calls it svg, sets w, h, id
	var svg = d3.select("#graph").append("svg")
			.attr("width", "100%")
			.attr("height", function(d) {
				return diameter * .90
				})
			.attr("id", "svg");

		// Creates graph title
		svg.append("text")
			.attr("id", "graph-title")
			.attr("margin-top", "100")
			.attr("x", "50")
			.attr("y", function(d){
				return diameter - (diameter * .925)})
			.attr("font-size", titleSize)
			.style("fill", "#2F4F4F")
			.text(title);

	// D3 packing layout
	var bubble = d3.layout.pack()
		.size([diameter, diameter])
		.value(function(d) {return d.size;})
		.sort(function(a, b) {
		    return -(a.value - b.value)
		}) 
		.padding(2);
	  
	// Generates data with calculated layout values
	var nodes = bubble.nodes(processData(json))
		.filter(function(d) { return !d.children; }); // filter out the outer bubble
	 
	// Select all nodes in svg (the container in the HTML)
	var vis = svg.selectAll(".node")
			.data(nodes);

	// For D3 transition effect
	var duration = 200;
	var delay = 0;

	// Enters the data into the HTML container, binding each data entry to a node, calls each one g
	var g = vis.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")" })
		.attr("id", "trendBubble");
		// debugger;

			g.append("circle")
				.attr("r", function(d) { return d.r; })
				.attr("class", function(d) { return d.name; })
				.style("opacity", 0)
				.style("fill", function (d) { return colorScale(d.size); })

				.transition()
				.duration(duration * 5)
				.style("opacity", .8)

			g.append("text")
				.attr("id", "p")
				.attr("dy", ".1em")
				.style("text-anchor", "middle")
				.text(function(d) { return d.name })
				.style("fill", "#2F4F4F")
				.style("font-size", function(d) {
					var len = d.name.substring(0, d.r / 3).length;
					var size = d.r/3;
					size *= 10 / len;
					size += 1;
					return Math.round(size)+'px'
					})
				.style("cursor", "pointer")
				.on("mouseover", function(d){d3.select(this)
					.style("font-size", "30px"
					)})
        		.on("mouseout", function(d){d3.select(this).style("font-size", function(d) {
        				var len = d.name.substring(0, d.r / 3).length;
						var size = d.r/3;
						size *= 10 / len;
						size += 1;
						return Math.round(size)+"px"
						})
        			})
				.on("click", function(d) {
							console.log("First")
					$("#tweets").css("display", "none")
						console.log("ERRR")
					$("#tweets").load("/status?trend=" + encodeURIComponent(d.name), function() {
						console.log("Third")
					$("#tweets").fadeIn("slow")
								}
							)});

		function processData(data) {

			var obj = data[Object.keys(data)[0]].ranked_trends;

			var newDataSet = [];

			for(var prop in obj) {
			console.log(prop)
		newDataSet.push({size: prop, name: obj[prop]});
			}
			return {children: newDataSet};
			}
	}
