""" Getting familiar with the Twitter API. Please pardon our dust. """

import twitter
import keyword_dict_creation
import working

#Authorization
auth = twitter.oauth.OAuth(working.OAUTH_TOKEN2, working.OAUTH_TOKEN_SECRET2, working.CONSUMER_KEY2, working.CONSUMER_SECRET2)

twitter_api = twitter.Twitter(auth=auth)

# #End authorization, beginning of Twitter search
# US_WOE_ID = 23424977

# # twitter_stream = TwitterStream(auth=auth, domain='stream.twitter.com')
# # for t in twitter_userstream.user():
# #     if 'direct_message' in msg:
# #         print msg['direct_message']['text']

# # Prefix ID with the underscore for query string parameterization.
# # Without the underscore, the twitter package appends the ID value
# # to the URL itself as a special case keyword argument.

# #Request for US trends according to ID
# us_trends = twitter_api.trends.place(_id=US_WOE_ID)

# #TESTING:
# # print world_trends
# # print
# # print us_trends

# #Creates trends list without duplicates
# us_trends_set = set([trend['name'] 
# 	for trend in us_trends[0]['trends']])

# #TESTING:
# # print "*****"
# # print "US Trends SET:", us_trends_set

# us_trends_list = []

# #Makes a list
# for i in us_trends_set:
# 	us_trends_list.append(i)

# #TESTING:
# print
# print "*************"
# print "US Trends LIST:", us_trends_list
# print "*************"
# print

# #Creates dictionary of trends(k) and the 15 requested tweets for them(v)
# # def make_tweet_text_dict():

# trend_text = {}

# # for trend in us_trends_list:

# search_result = twitter_api.search.tweets(q='dog', count=5)

# for item in search_result['statuses']:
# 	print
# 	print "THIS IS THE ITEM *****", item

# print "SCREEN NAME:", search_result['statuses'][0]['user']['screen_name']

# print "***********"

# print "TEXT:", search_result['statuses'][0]['text']

# print "***********"

# print "ID:", search_result['statuses'][0]['id_str']

# print "***********"

# print "SEARCH RESULT:", search_result

# trend_tweets = []

# tweet_entities = ()

# for item in search_result['statuses']:
# 	tweet_entities = (item['user']['screen_name'], item['text'], item['id_str'])
# 	trend_tweets.append(tweet_entities)

# print type(tweet_entities)
# print trend_tweets



# # ['description']
# # trend_text[trend] = []
	
# # for item in search_result['statuses']:
# # 	text = item['text']
# # 	trend_text[trend].append(text)
# # 	print "TREND TEXT:", trend_text

# # trend_tweets = make_tweet_text_dict()


# # print
# # print "*************"
# # print "STREAMING STUFF:", streaming_stuff
# # print "*************"
# # print

# #TESTING
# # words = [ w for t in status_texts for w in t.split() ]

# #TESTING:
# # print make_tweet_text_dict()['Happy Chinese New Year']
# # print len(make_tweet_text_dict()['Happy Chinese New Year's])

# #TESTING
# # test_dict = {'I': ['new', 'year']}

# # keyword_dict = keyword_dict_creation.make_dict()

# # word_count = 0

# # for k, v in trend_tweets.items():
# # 	for item in trend_tweets[k]:
# # 		print item

# # for k, v in trend_tweets.items():
# # 	for v in trend_tweets[k]:
# # 		words = v.split()
# # 		for word in words:
# # 			for x, y in keyword_dict.items():
# # 				if word.lower() == x:
# # 					print k
# # 					word_count = word_count + 1
# # 				if word.lower() == y:
# # 					print k
# # 					word_count = word_count + 1

# # print word_count

# # words = [ w for t in status_texts for w in t.split() ]

# # for word in words:
# #   word.encode('ascii', 'ignore')
# #   if word.lower() == "dies":
# #     keyword_count = keyword_count + 1

# # print
# # print "********"
# # print "Number of times word 'dies' appears:", keyword_count

# trend = "dog love"

# print "THIS IS THE TREND:", trend

# auth = twitter.oauth.OAuth(working.OAUTH_TOKEN, working.OAUTH_TOKEN_SECRET, working.CONSUMER_KEY, working.CONSUMER_SECRET)

# twitter_api = twitter.Twitter(auth=auth)

# search_result = twitter_api.search.tweets(q=trend, lang="en", result_type="mixed", count=5)

# print "********"
# print "SEARCH RESULT:", search_result
# print "********"

# tweet_id = search_result['search_metadata']['max_id_str']

# print "SEARCH RESULT STATUS:", tweet_id

# search_result_2 = twitter_api.search.tweets(q=trend, lang="en", result_type="mixed", since_id="575383301222957056", count=5)

# print
# print "**********"
# print "NEXT SEARCH RESULTS:", search_result_2
# print "**********"

# trend_tweets = []

# tweet_entities = ()

# trend_tweets_dict = {}

# for item in search_result['statuses']:
# 	tweet_entities = (item['user']['screen_name'], item['text'], item['id_str'])
# 	trend_tweets.append(tweet_entities)

# trend_tweets_dict['tweets'] = trend_tweets

# status_check = twitter_api.trends.place(_id=2487956)

# check_check = status_check.headers["status"]

try:
	fake_search = twitter_api.trends.place(_id=1)
	print fake_search
except twitter.api.TwitterHTTPError:
	print "Bummer"