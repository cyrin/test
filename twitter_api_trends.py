""" Receives and calls Twitter trends for USER_INPUT. Outputs ranked trends, searches for Tweets according to trends """

import twitter
import working

#Authorization
auth = twitter.oauth.OAuth(working.OAUTH_TOKEN, working.OAUTH_TOKEN_SECRET, working.CONSUMER_KEY, working.CONSUMER_SECRET)

twitter_api = twitter.Twitter(auth=auth)

#End authorization, beginning of Twitter search
US_WOE_ID = 23424977 #WoE ID for United States
SF_WOE_ID = 2487956 #WoE ID for San Francisco

USER_INPUT = 2487956 #Whatever you get from the form, set to San Francisco for now

#Request for US trends according to ID
# us_trends = twitter_api.trends.place(_id=US_WOE_ID)
sf_trends = twitter_api.trends.place(_id=SF_WOE_ID)
# user_trends = twitter_api.trends.place(_id=USER_INPUT) #This will happen eventually

# TESTING: Prints big scary objects for each request
# print "*********"
# print "US TRENDS:", us_trends
# print "*********"
# print "SF TRENDS:", sf_trends
# print "*********"

#Creates trends set without duplicates, list comprehension
# us_trends_set = set([trend['name'] for trend in us_trends[0]['trends']])

sf_trends_set = set([trend['name'] for trend in sf_trends[0]['trends']])

us_trends_list = []
sf_trends_list = []

#Makes a list, list comprehension
# us_trends_list = [i for i in us_trends_set]
sf_trends_list = [i for i in sf_trends_set]

#TESTING:
print
print "*************"
# print "US Trends LIST:", us_trends_list
print "SF Trends LIST:", sf_trends_list
print "*************"
print

ranking_list = sorted([x + 1 for x in range(10)], reverse=True)

us_trends_ranked = {}
sf_trends_ranked = {}
sf_trends_ranked['ranked_trends'] = {}

#This can go to the webpage
sf_trends_ranked['ranked_trends'] = dict(zip(ranking_list, sf_trends_list))
print sf_trends_ranked

#Creates dictionary of trends(k) and the 15 requested tweets for them(v)
# def make_tweet_text_dict():

# 	trend_text = {}

# 	for trend in us_trends_list:
# 		search_result = twitter_api.search.tweets(q=trend, count=15)

# 		trend_text[trend] = []
		
# 		for item in search_result['statuses']:
# 			text = item['text']
# 			trend_text[trend].append(text)

# 	return trend_text

# trend_tweets = make_tweet_text_dict()
