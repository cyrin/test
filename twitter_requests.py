import twitter
import ENV

#Global variables for OAuth and instatiating the Twitter class
AUTH = twitter.oauth.OAuth(ENV.OAUTH_TOKEN, ENV.OAUTH_TOKEN_SECRET, ENV.CONSUMER_KEY, ENV.CONSUMER_SECRET)

TWITTER_API = twitter.Twitter(auth=AUTH)

def ranked_trend_list(city):
	"""Uses the Twitter API to create and return a dictionary of top 10 trends ranked"""

	ranking_list = sorted([x + 1 for x in range(10)], reverse=True)

	trends_ranked = {}

	woeid = city

	trends_list = []

	trends_ranked["ranked_trends"] = {}

	trends = TWITTER_API.trends.place(_id=city)

	trends_set = set([trend["name"] for trend in trends[0]['trends']])

	trends_list = [i for i in trends_set]

	trends_ranked["ranked_trends"] = dict(zip(ranking_list, trends_list))

	return trends_ranked

def trend_tweets(trend, max_id=None):
	"""Uses the Twitter API to return a list of tweets and tweet entities depending on the selected trend"""

	trend_tweets = []

	tweet_entities = ()

	trend_tweets_dict = {}

	search_result = TWITTER_API.search.tweets(q=trend, count=6, lang="en", result_type="mixed", since_id=max_id)

	# print search_result

	for item in search_result["statuses"]:
		tweet_entities = (item["user"]["screen_name"], item["text"], item["id_str"])
		trend_tweets.append(tweet_entities)

	trend_tweets_dict["tweets"] = trend_tweets

	return search_result
